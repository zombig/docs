/*README!
  запуск:
  node tests/spellcheck/spellCheck.js

  нашли что-то проверяем слово например здесь
  https://languagetool.org/ru/
  если все верно - значит добавляем его в словарь

  еще он не понимает даты 2017-01-05 и некоторые спецсимволы - значит добавляем в excludes (ищи ниже)
*/

const excludes = [
  'yH5BAAAAAAALAAAAAAQAA4AAARe8L1Ekyky67QZ1hLnjM5UUde0ECwLJoExKcppV0aCcGCmTIHEIUEqjgaORCMxIC6e0CcguWw6aFjsVMkkIr7g77ZKPJjPZqIyd7sJAgVGoEGv2xsBxqNgYPj',
  'eyJpcCI6ICIxMjcuMC4wLjEiLCAiZ3VpZCI6ICJkMDU4YTE4YTlmN2U0YTdkYmFjMjBhMWVhNTE5YmViYSIsICJ1c2VyX2lkIjogMjJ9',
  '6772cc1fd3b9f735b3c3339535f86bdef741bea2',
  '3e3c98dbca8dae4e57aa2705a548a58243ecabb4',
  'search-tamtam-ubrrgoikcgxj64dsn3pbdbyyeq',
  '66fc898871f3496d8762f27f44845ad4',
  '52484a7d571e43799d4ae562a93bb794',
  '4faf5a45c5ba447ab7004b30181dd4c1',
  '8eee728938914cf7a1ecaaca66d45ac6',
  '---------------573cf973d5228--',
  'R0lGODlhEAAOALMAAOazToeHh0tLS',
  '---------------573cf973d5228',
  '-------------573cf973d5228',
  'xxxabracadabra',
  '3kdbW1mxsbP',
  'abf885a890',
  'gAwXEQA7',
  '0jvb29t',
  'ge8WSLf',
  '-H', '-X', '-F', '-c', '_', '-m', 'f3', 'Ub', 'rhf', '-I', '-d', '-C','-L',
  'ГГГГ-ММ-ДД', '2019-11-11T11',  '2017-05-14', '2019-02-21T07',
  'rrggbb', 'ffff00', '0000ff',
  '--autogenerate', '-xvpzf',
  '123Z', '955918Z',  'ЧЧ',
  '7LZv', 'x3A',
];



/*globals require, global*/
const spell = require('spell-checker-js');
const remark = require('remark')
const strip = require('strip-markdown')

const fs = require('fs');

const expeptDirs = ['.git', 'node_modules', 'tests']

spell.load('en');
spell.load('ru');
spell.load('./tests/spellcheck/dict.en.txt');
spell.load('./tests/spellcheck/dict.ru.txt');




const clearFile = async (file)=> {
  return new Promise((resolve)=> {
    remark()
      .use(strip)
      .process(file, function(err, file) {
        if (err)
          throw err

        resolve(String(file));
      });
  });
}

const checks = [];
const iterate = async (rootpath)=> {
  const files = fs.readdirSync(rootpath);
  for (fpath of files) {
    const fullPath = `${rootpath}/${fpath}`

    if (fs.lstatSync(fullPath).isDirectory()) {
      if (expeptDirs.indexOf(fpath) === -1)
        await iterate(rootpath + '/' + fpath);

      continue;
    }


    const splitted = fpath.split('.');
    const ext = splitted[splitted.length - 1];
    if (!ext || ext.toLowerCase() !== 'md')
      continue;

    const file = fs.readFileSync(fullPath, 'utf8');
    const clearedFile = await clearFile(file);

    const check = spell.check(clearedFile)

    for (let z of check) {
      let zn = Number(z);
      if (typeof(zn) !== 'number' || isNaN(zn))
        if (excludes.indexOf(z) === -1)
          checks.push(z);
    }
  }
}


iterate('.')
.then(()=> {
  if (checks.length) {
    console.log('bad words?', checks); //eslint-disable-line
    process.exit(-1); //eslint-disable-line
  }
});
