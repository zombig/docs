Бот - это специализированный тип пользователя. Под ним нельзя залогиниться, а управлять может создатель через InfoBot-а. Бот подчиняется общим правам системы, т.е. должен быть добавлен в чат в который должен писать/читать. Боту передается текст сообщений обращенных к нему, если задан обратный url. Читать все сообщения чата бот не может.


### Создание и управление ботом

Создавать и менять настройки бота можно с помощью бота InfoBot в своем Info Chat-е.

Создаем бота командой `@infobot new_bot helpbot ChatHelpBot`, где первый аргумент уникальное имя, которое должно оканчиваться на `bot`, и читаемое имя.

В ответ придет сообщение с ключом АПИ вида

`as3afgmkkmmepfbosv6o6hpph8s3fj2b9kpd0jutpts3i9ct67u`, где

`URL_KEY = as3afgmk`, первые 8 символов используются при создании url бота 

`API_KEY = 6hpph8s3fj2b9kpd0jutpts3i9ct67u`, отбросим первые 20 символом и получим ключ АПИ, для отправки постов и доступа к другим АПИ (через заголовок `X-APIToken`).

Установить url на который будут приходить обращения к боту можно командой

`@infobot set_bot helpbot url http://lwr.pw/as3afgmk`, где `helpbot` - уникальное имя бота, `url` - редактируемая опция, `http://lwr.pw/as3afgmk` - значение опции, адрес, куда будут отправляться сообщения.

Другие команды можно посмотреть отправив `@infobot help`


### Отправка сообщений

`POST /bot/message`

```javascript
{
    "chat_id": 37,  // int
    "reply_no": null,  // int or null
    "text": "@all test" // text, 10k symbols limit
}
```

200 OK

```javascript
{
    "post_no": 123
}
```

Например,

```bash
/bin/echo '{"chat_id": 37, "text": "@all test"}' | curl -H "X-APIToken: 6hpph8s3fj2b9kpd0jutpts3i9ct67u" -H "Content-Type: application/json" -v -X POST -k -d @- https://api.pararam.io/bot/message
```

#### Обработка ошибок

Ошибки доступа

```javascript
{
    "code": "access_deny"
    "message": "...some text..."
}
```

Ошибки валидации

Можно определить по флагу `"tamtam_response_api": true`

```javascript
{
    "tamtam_response_api": true,
    "codes": {"<field_name>|non_field": "str_code"},
    "field_errors": {"<field_name>|non_field": "str_message"}
    "extra": {}, // optioanl
    "error": "str_message"
}
```


### Отправка сообщений в приватный чат

По user_id

`POST /msg/post/private`

```javascript
{
    "text": "some text",
    "user_id": 12
}
```

200 OK

```javascript
{
    "chat_id": 23,
    "post_no": 123
}
```

По email

`POST /msg/post/private`

```javascript
{
    "text": "some text",
    "user_email": "user@example.com"
}
```

200 OK

```javascript
{
    "chat_id": 23,
    "post_no": 123
}
```


### Получение сообщений

На адрес установленный командой `@infobot set_bot helpbot url http://lwr.pw/as3afgmk` POST-ом отправляется сообщение в формате json

```javascript
{
    "user_id": 12,  // int
    "chat_id": 21,  // int
    "post_no": 11,  // int
    "text": "some text",  // str
    "organization_id": null,  // null or int
    "reply_no": 7,  // optional, int
    "reply_text": "some text"  // optional, str
}
```

и заголовок `X-Signature` - подпись пакета, ее можно проверить так:

```python
BKEY = to_bytes(int('<key>', 32))

def _make_signature(body: str):
    return str(base64.b64encode(
        hmac.new(BKEY, bytes(body, 'utf8'), digestmod=hashlib.sha256).digest()
    ), 'utf8')

if request.headers['x-signature'] != _make_signature(await request.text()):
    print('Check failed')
```


### Задачи

Получение списка задач

`GET /msg/task`

```javascript
{
  "tasks": [
    {
      "chat_id": 44877,
      "post_no": 73,
      "state": "open",
      "time_created": "2019-10-15T10:20:36.842545Z",
      "time_updated": "2019-10-15T10:20:36.842545Z"
    },
    ...
  ]
}
```

Изменение статуса

`POST /msg/task/{chat_id}/{post_no}`

```javascript
{
    "state": "open"  // open, done, close
}
```


Например,

```bash
/bin/echo '{"state": "done"}' | curl -H "X-APIToken: 6hpph8s3fj2b9kpd0jutpts3i9ct67u" -H "Content-Type: application/json" -v -X POST -k -d @- https://api.pararam.io/msg/task/37/123
```


## Основные АПИ

Пробная [OpenApi3](https://tamtamfiles-public.s3-eu-west-1.amazonaws.com/openapi/v2/openapi.yaml) спецификация (могут быть отступления в ответах, ValidationError мало описан). Можно открыть в https://editor.swagger.io/ и использовать в качестве документации.


### Пользователи

Зная id пользователя можно запросить общую информацию о нем `GET /core/user?ids=11,12,15`.


### Чаты

Получить данные всех доступных чатов можно в 2 приема, сначала по `GET /core/chat/sync` получить список id всех доступных чатов (все независимо от статуса видимости).
Потом по списку id получить данные чатов `GET /core/chat?ids=11,12,15`.

Так же бот может создавать/редактировать/удалять чаты в соответствии со своими правами.
Редактирование состава участников чата ботам не доступно.

Для работы с приватными чатами, есть отдельное АПИ `POST /core/chat/pm/{user_id}`, которое возвращает id приватного чата, существующего или вновь созданного.

Так же бот может входить/выходить в/из чатов, скрывать ненужные, добавлять/убирать из избранных, форкать.

### Команды

Получить данные всех доступных команд можно в 2 приема, сначала по `GET /core/org/sync` получить список id. Потом по списку id получить данные команд `GET /core/org?ids=11,12,15`.

Так же бот может создавать/редактировать/удалять чаты в соответствии со своими правами.
Редактирование состава участников чата ботам не доступно.

Так же бот может входить в команды куда его пригласили и покидать их.