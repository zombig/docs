# Как зарегистрировать аккаунт.

![Главный экран](../img/main_screen.png)

*  На главной странице [Pararam](https://app.pararam.io/login#login-buttons) нажать кнопку "Register"



*  На открывшемся экране заполнить регистрационную форму и нажать "Register"

![Регистрационная форма](../img/reg_form.png)

*  На указанную электронную почту будет выслано письмо с подтверждением регистрации, о чём будет сообщено в текстовом сообщении. 
>  Account successfully registered. Email with activation link was sent to your email box. 
*  В письме должен содержаться следующий текст:
>  Hello (имя пользователя)!
Thank you for signing up with Pararam. To activate
your newly created account, please click on the following link:

[пример ссылки] (https://app.pararam.io/login?activation_key=1615b38ce46946c1be4dbde653807c11)

* Для активации аккаунта переходим по ссылке из письма.
*  В открывшемся окне получаем подтверждение что аккаунт зарегистрирован в виде текстового сообщения.
>  You have successfully activated your account. Please enter your password to sign in!
*  Поля "**Email**" и "**Password**" должны уже быть заполнены.
*  Для входа в программу нажать кнопку "**Sign in with Pararam**".
