# Docs


[Как зарегистрировать новый аккаунт](./Sign_in/How_to_register_a_new_account.md)

[Как войти с использованием Google аккаунта](./Sign_in/Sign_in_with_Google_account.md)

[Попробовать Pararam](./Sign_in/Try_Pararam.md)

[Команды на главном экране](./main_screen/team.md)

[Лента чатов](./main_screen/chat_feed.md)

[Активный чат](./main_screen/main_chat_screen.md)

[Блок контактов](./main_screen/contacts_block.md)

[Новый чат](./chat/New_chat.md)

[Пригласить друга](./invite/Invite_frends.md)

[Пользователи на экране настройки команды](./teams/main_screen_team_users.md)

[Гости](./teams/guests.md)

[Чаты](./teams/chats.md)

[Группы](./teams/groups.md)

[Настройки команды](./teams/settings.md)

[Рассылка](./teams/mailing.md)

[Статусы пользователей](./teams/user_status.md)

[Назад в Pararam](./teams/back_to_pararam.md)

[Сведения о пользователе](./profile/information.md)

[Настройки](./profile/settings.md)

[Командные статусы](./profile/team_status.md)

[Настройки чата (Сведения)](./chat/chat_settings/chat_info.md)

[Настройки чата (Настройки)](./chat/chat_settings/settings.md)

[Настройки чата (Правка)](./chat/chat_settings/editing.md)

[Настройки чата (Приглашения)](./chat/chat_settings/invitations.md)

[Настройки чата (Управление участниками)](./chat/chat_settings/manage_members.md)

[Настройки чата (Удаление)](./chat/chat_settings/removel.md)

[Боты](./bots/bots.md)

[Звонки](./calls/call.md)

[Нормальные и тихие звонки](./calls/normal_call_silent_call.md)

[Видеозвонки](./calls/videocall.md)

[Остальное](./others/README.md)
