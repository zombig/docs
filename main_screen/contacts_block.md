# Блок контактов и участников чата

![Текущие пользователи чата](../img/chat_members.png)

----

- Пункт "**Текущие пользователи чата**" 

  - В этом пункте отображается общее количество участников выделенного чата, сколько из них "онлайн" в данный момент.
  - Есть кнопка "**Файлы чата**", открывающее окно с всеми фалами загруженными в этот чат. 
  - Есть строка поиска пользователей.
  - Нажатие на пользователя в этом листе открывает окно с подробностями его профиля.

  ![Профиль пользователя чата](../img/user_details.png)

  - Наведение курсора показывает окошко с информацией о пользователе.
  
  ![Текущие пользователи чата](../img/profile_window.png)

  ----

- Пункт "**Контакты**"
  - В этом пункте показаны контакты залогиненного пользователя.

  ![Текущие пользователи чата](../img/contacts.png)

  - Указано общее количество контактов и сколько из них "в сети".
  - Есть кнопка "**Пригласить друзей**", нажатие на которую открывает окно с функционалом приглашения.

  ![Пригласить друзей](../img/invite_friends.png)
  - Есть строка поиска контактов.

----

- Пункт "**Команда**"

  ![Команда](../img/team.png)

  - Если команда не выделена или пользователь не состоит в команде, для него отображается только стандартная строка поиска участников.

  - Если пользователь участник команды и команда выделена;

  ![Выделенная команда](../img/team_members.png) 

  - Для пользователя отображаются кнопки:
    - "**Файлы команды**" - кнопка идентичная по функционалу описанной ранее "**Файлы чата**, только с применением к команде.
    - "**Создать новую группу**" - нажатие открывает окно создания группы пользователей внутри команды 

    ![Новая группа](../img/new_group.png)

    - "**Управление командой**" - кнопка открывающая отдельное окно браузера с функционалом настройки и управления командой. (Подробнее в соответствующем разделе)

    - По мимо участников команды, в списке отображены названия находящихся в ней групп.
    - По нажатию на группу открывается окно управления группой.

    ![Окно управление группой](../img/group_info.png)

----

- Пункт "**Заблокированные пользователи**"

![Заблокированные пользователи](../img/blocked_users.png)
  
  - В этом пункте отображаются аккаунты со статусом "Заблокирован" у залогиненного пользователя. Когда аккаунт получает такой статус, все ранее созданные с ним чаты исчезают для пользователя заблокировавшего аккаунт.
  - Если таких пользователей нет, показан пустой список "**У вас нет заблокированных пользователей**"
  - Если такие пользователи есть, то по нажатию на него появится окно профиля, в котором можно отменить блокировку.

 ![Заблокированный пользователь](../img/blocked_user.png)
  
  - Так же пункт имеет стандартную форму поиска контактов.

  ----

- "**Верхняя часть блока**"

  ![Верхняя часть блока](../img/upper_part.png)

  - В этой части размещены три пункта:
    - "**Упоминания(Ответы)**"
    - "**Напоминания и закладки**"
    - "**Аватарка пользователя с меню управления Pararam**" 

  - "**Упоминания(Ответы)**" - открывает одноименное окно с подробностями.

   ![Упоминания(Ответы)](../img/mentions.png)

   - "**Напоминания и закладки**" - так же открывает окно с подробностями.

   ![Напоминания и закладки](../img/reminders.png)

   - "**Аватарка пользователя с меню управления Pararam**" - нажатие на аватарку показывает ниспадающее меню с настройками и пунктами управления Pararam.

   ![Настройки](../img/user_profile_menu.png)

----

- "**Стрелка**"

  - Нажатие на стрелку полностью скрывает с экрана весь блок контактов, если он не скрыт, и наоборот если иначе.